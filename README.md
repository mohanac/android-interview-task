# Influx Task - Android

Compelete this task to score your interview with Influx Worldwide

You will have to build an android app with one activity as per designed attached, can use MVVM or Architecture components.

**P.S.** Bonus Points, for large and diverse test code coverage with few espresso UI or Junit test.

Few things to keep in mind, 

1) Slider UI should update based on add/remove of items

2) Amount should be calculated dynamically and updated appropriately based on add/remove of items

3) Some items have subitems(modifiers), they need to reflect accordingly on add/remove of items

4) We love Kotlin and hope you too do this project in Kotlin

[Consume this](http://www.mocky.io/v2/5b700cff2e00005c009365cf)

Design Requirements as below :

![Design](https://gitlab.com/mohanac/android-interview-task/raw/master/Design-1.jpg)

![Design](https://gitlab.com/mohanac/android-interview-task/raw/master/Slider-1.jpg)

 
Share your code repository link to mohana.srinivas@influx.co.in

Explore more : www.influxworldwide.com
